import _ from "lodash";

/**
 * 
 * @param {*} obj : object has array of classname
 * function: concat array classname to string classname
 * example: ["flex bg-red", "mt-8 mx-2"] => "flex bg-red mt-8 mx-2"
 */
const bind = (obj = {}) => {
    _.entries(obj).forEach(([key, value]) => {
        if (_.isArray(value)) {
            obj[key] = value.join(" ");
        }
    });
}

const Utils = {
    bind
}

export default Utils