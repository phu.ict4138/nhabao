import { useNavigate } from "react-router-dom";

import className from "./className";
import imageNotFound from "../../assets/images/not_found.png";

function Empty() {

    const navigate = useNavigate()

    return (
        <div className={className.container}>
            <div className={className.content}>
                <img
                    className={className.image}
                    src={imageNotFound} />
                <div className={className.title}>
                    Trang bạn yêu cầu không tồn tại
                </div>
                <div
                    className={className.button}
                    onClick={() => navigate("./")}>
                    Quay lại trang chủ
                </div>
            </div>
        </div>
    )
}

export default Empty