import Utils from "../../utils/Utils";

const className = {
    wrapper: [
        "flex flex-col bg-blue-300 h-8 ml-2",
        "invisible",
        "xl:w-4/12 xl:visible"
    ]
}

Utils.bind(className);

export default className