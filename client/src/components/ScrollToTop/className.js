import Utils from "../../utils/Utils"

const className = {
    wrapper: [
        "fixed z-10 bottom-10 right-10 cursor-pointer",
        "flex items-center justify-center",
        "text-blue-400 text-3xl",
        "bg-gray-200 w-12 h-12 rounded-lg hover:bg-gray-100"
    ],
    show: " visible",
    hidden: " invisible"
}

Utils.bind(className);

export default className