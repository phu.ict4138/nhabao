import { faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import config from "../../utils/Config";
import className from "./className";

const navBars = [
    {
        id: 0,
        display: "TRANG CHỦ",
        path: "/"
    },
    {
        id: 1,
        display: "MỚI",
        path: "/newest"
    },
    {
        id: 2,
        display: "VIDEO",
        path: "/video"
    },
    {
        id: 3,
        display: "CHỦ ĐỀ",
        path: "/category"
    },
]

function NavBar() {

    const navigate = useNavigate();
    const [on, setOn] = useState(false)
    const getCurrentIDPage = () => {
        const url = window.location.href;
        if (url == config.baseURL) {
            return 0;
        }
        for (const item of navBars) {
            const index = url.indexOf(item.path.slice(1, item.path.length));
            if (index > 0) {
                return item.id;
            }
        }
        return -1;
    }

    const handleShow = () => {
        setOn(!on)
    }

    const idCurrentPage = getCurrentIDPage();

    const navs = navBars.map(item => ({
        ...item,
        active: item.id == idCurrentPage
    }));

    return (
        <div className={className.wrapper}>
            <div className={className.content}>
                <div className="flex-1 flex flex-row">
                    {navs.map(item => (
                        <div
                            key={item.id}
                            className={className.navItem + (item.active ? className.active : "")}
                            onClick={() => navigate(item.path)}>
                            {item.display}
                        </div>
                    ))}
                </div>

                <div onClick={handleShow}>
                    <FontAwesomeIcon icon={faBars}
                        className={"text-white text-2xl mr-6 "} />
                </div>
            </div>
            {on ?
                (<div className="flex-1 flex flex-row flex-wrap w-full bg-slate-200">
                    <div onClick={() => {
                        navigate("/category/thoisu-1")
                        setOn(!on)
                    }}
                        className="w-1/4 text-xl text-center m-3">
                        Thời sự
                    </div>
                    <div
                        onClick={() => {
                            navigate("/category/kinhdoanh-2")
                            setOn(!on)
                        }}
                        className="w-1/4 text-xl text-center m-3">Kinh doanh</div>
                    <div
                        onClick={() => {
                            navigate("/category/giaoduc-3")
                            setOn(!on)
                        }}
                        className="w-1/4 text-xl text-center m-3">Giáo dục</div>
                    <div
                        onClick={() => {
                            navigate("/category/thethao-4")
                            setOn(!on)
                        }}
                        className="w-1/4 text-xl text-center m-3">Thể thao</div>
                    <div
                        onClick={() => {
                            navigate("/category/suckhoe-5")
                            setOn(!on)
                        }} className="w-1/4 text-xl text-center m-3">Sức khoẻ</div>
                    <div
                        onClick={() => {
                            navigate("/category/giaitri-6")
                            setOn(!on)
                        }}
                        className="w-1/4 text-xl text-center m-3">Giải trí</div>
                    <div
                        onClick={() => {
                            navigate("/category/thegioi-7")
                            setOn(!on)
                        }}
                        className="w-1/4 text-xl text-center m-3">Thế giới</div>
                    <div
                        onClick={() => {
                            navigate("/category/phapluat-8")
                            setOn(!on)
                        }}
                        className="w-1/4 text-xl text-center m-3">Pháp luật</div>
                    <div
                        onClick={() => {
                            navigate("/category/doisong-9")
                            setOn(!on)
                        }}
                        className="w-1/4 text-xl text-center m-3">Đời sống</div>
                    <div
                        onClick={() => {
                            navigate("/category/xe-10")
                            setOn(!on)
                        }}
                        className="w-1/4 text-xl text-center m-3">Xe</div>
                    <div
                        onClick={() => {
                            navigate("/category/khoahoc-11")
                            setOn(!on)
                        }}
                        className="w-1/4 text-xl text-center m-3">Khoa học</div>
                </div>)
                : <></>}
        </div>
    );
}

export default NavBar
