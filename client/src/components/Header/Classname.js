import Utils from "../../utils/Utils"

const className = {
    wrapper: [
        "w-screen",
        "flex",
        "flex-col",
        "h-20",
        "items-center",
        "justify-center"
    ],
    content: "flex flex-row w-screen justify-between xl:w-xl",
    logo: "h-10 cursor-pointer ml-4 xl:ml-0",
    action: ""

}

Utils.bind(className)

export default className