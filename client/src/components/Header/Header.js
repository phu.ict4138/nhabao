import { useNavigate } from "react-router-dom";

import SearchBar from "../SearchBar";
import classmame from "./Classname";


function Header() {

    const navigate = useNavigate();

    return (
        <div className={classmame.wrapper}>
            <div className={classmame.content}
                style={{
                }}>
                <img
                    className={classmame.logo}
                    onClick={() => navigate("/")}
                    src="https://baomoi-static.bmcdn.me/web-v2/prod/v0.3.23/public/images/default-skin/bm-logo.png" />
                <SearchBar />
                <div className={classmame.action}>

                </div>
            </div>
        </div>
    );
}

export default Header