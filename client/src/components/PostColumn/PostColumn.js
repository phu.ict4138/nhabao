import className from "./className"
import Post from "./Post"


function PostColumn({ posts = [] }) {

    return (
        <div className={className.wrapper}>
            <div className={className.content}>
                {posts.map(post => (
                    <Post
                        key={post.id}
                        {...post} />
                ))}
            </div>
        </div>
    )
}

export default PostColumn