import Utils from "../../utils/Utils"

const className = {
    wrapper: [
        "w-full"
    ],
    content: [
        "flex flex-col"
    ],

}

Utils.bind(className);

export default className