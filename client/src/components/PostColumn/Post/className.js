import Utils from "../../../utils/Utils";

const className = {
    wrapper: [
        "w-full flex flex-row",
        "mt-4",
        "py-2",
    ],
    image: [
        "w-1/3",
        "md:w-1/4",
        "cursor-pointer"
    ],
    content: [
        "ml-3"
    ],
    title: [
        "text-base",
        "md:text-xl",
        "cursor-pointer",
    ],
    infor: [
        "flex flex-row items-center",
        "mt-2"
    ],
    logoPaper: [
        "h-4"
    ],
    time: [
        "ml-2",
        "text-gray-500 text-base",
    ],
}

Utils.bind(className);

export default className