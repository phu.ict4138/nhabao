import Utils from "../../utils/Utils"

const className = {
    wrapper: [
        "w-full",
        "mt-2 py-2",
        "flex flex-row",
        "md:flex-col md:w-1/3 md:px-2",
    ],
    image: [
        "w-1/3",
        "md:w-full md:h-60 object-cover",
        "cursor-pointer"
    ],
    content: [
        "w-2/3 pl-2",
        "flex flex-col",
        "md:w-full md:mt-2"
    ],
    title: [
        "leading-5",
        "line-clamp-3",
        "cursor-pointer",
    ],
    infor: [
        "flex flex-row",
        "items-center",
        "mt-[4px]"
    ],
    logoPaper: [
        "h-4"
    ],
    time: [
        "text-gray-500 text-xs",
        "ml-2"
    ],
}

Utils.bind(className);

export default className