import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import PostColumn from "../../components/PostColumn";
import className from "../Home/className";
function CategoryDetail() {
    const [number, setNumber] = useState(15)
    const { categoryId } = useParams();
    const id = categoryId.split("-")[1]
    const [ids, setIds] = useState(id)
    const [datas, setDatas] = useState([])

    const loaddata = async () => {
        const data = await fetch(`http://localhost:7075/api/v1/post/search?limit=${number}&categoryId=${id}`)
            .then((response) => response.json())
        setDatas(data.posts)
    }

    useEffect(() => {
        loaddata()
    }, [number, ids])

    useEffect(() => {
        if (id != ids) {
            setIds(id)
            setDatas([])
        }
    })
    console.log(number);
    const handleLoad = () => {
        setNumber(number + 15)
    }

    return (
        <div className={className.container}>
            <div className={className.content}>
                {datas.length == 0 ? "Chưa có bài viết!" : (
                    <>
                        <PostColumn
                            posts={datas} />
                    </>)}

            </div>

            <div className={className.button}
                onClick={handleLoad}
                style={{ display: (number >= datas.length) ? "none" : "" }}
            >
                Loadmore
            </div>
        </div >
    );
}

export default CategoryDetail