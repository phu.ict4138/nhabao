import Utils from "../../utils/Utils";

const className = {
    container: [
        "w-screen xl:w-xl flex flex-col pt-4"
    ],
    content: [
        "flex flex-col w-full px-4",
        "xl:w-8/12 xl:px-0"
    ],
    button: [
        "text-center",
        "text-xl",
        "text-black	",
        'py-2',
        'px-20',
        "bg-teal-500",
        "cursor-pointer"
    ]
}

Utils.bind(className);

export default className