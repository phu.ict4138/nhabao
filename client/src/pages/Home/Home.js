import PostColumn from "../../components/PostColumn";
import SideBar from "../../components/SideBar";
import PostMain from "./components/PostMain";
import { useState, useEffect } from "react";
import className from "./className";


function Home() {
    const [number, setNumber] = useState(0)
    const [posts, setPosts] = useState([])
    const loaddata = async () => {
        const data = await fetch(`http://localhost:7075/api/v1/home/get-home-page?page=${number}`)
            .then((response) => response.json())
        setPosts([...posts, ...data.posts])
    }

    const handleLoad = () => {
        setNumber(number + 1)
    }
    useEffect(() => { loaddata() }, [number])

    return (
        <div className={className.container}>
            <div className={className.content}>
                {posts.length == 0 ? "Chưa có bài viết!" : (
                    <>
                        <PostMain
                            posts={posts} />
                        <PostColumn
                            posts={posts} />
                    </>)}

            </div>
            <div className={className.button}
                style={{ display: (number >= posts.length) ? "none" : "" }}
                onClick={handleLoad}
            >
                Loadmore
            </div>
        </div>
    )
}

export default Home
