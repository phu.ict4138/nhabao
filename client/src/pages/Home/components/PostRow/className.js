import Utils from "../../../../utils/Utils";

const className = {
    wrapper: [
        "flex flex-col w-full",
        "md:flex-row md:-ml-2 md:-mr-5",
    ],
}

Utils.bind(className);

export default className