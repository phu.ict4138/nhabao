import Post from "../../../../components/Post"
import className from "./className"

function PostRow({ posts = [] }) {

    return (
        <div className={className.wrapper}>
            {posts.map(post => (
                <Post
                    key={post.id}
                    {...post} />
            ))}
        </div>
    )
}

export default PostRow