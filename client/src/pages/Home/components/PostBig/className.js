import Utils from "../../../../utils/Utils";

const className = {
    wrapper: [
        "flex flex-col"
    ],
    image: [
        "w-full",
        "cursor-pointer",
    ],
    content: [
        "flex flex-col min-h-[4rem] mt-1",
        "md:min-h-0 xl:min-h-0"
    ],
    title: [
        "cursor-pointer",
    ],
    infor: [
        "flex flex-row items-center"
    ],
    logoPaper: [
        "h-4"
    ],
    time: [
        "text-gray-500 text-xs",
        "ml-2"
    ]
}

Utils.bind(className);

export default className