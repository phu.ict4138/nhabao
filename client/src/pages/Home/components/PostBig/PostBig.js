import className from "./className"

function PostBig({ title, image, publish, url, paper }) {
    const today = new Date()
    const publishTime = new Date(publish)
    const a = (today - publishTime)
    let b = ''
    if (a < 60000) {
        b = `${Math.floor(a / 1000)} giay trước`
    } else if (a < 3600000) {
        b = `${Math.floor(a / 60000)} phút trước`
    } else if (a < 86400000) {
        b = `${Math.floor(a / 3600000)} giờ trước`
    } else if (a < 2592000000) {
        b = `${Math.floor(a / 86400000)} ngày trước`
    } else if (a < 31104000000) {
        b = `${Math.floor(a / 2592000000)} tháng trước`
    } else if (a > 31104000000) {
        b = `${Math.floor(a / 31104000000)} năm trước`
    }
    const handleClick = () => {
        const a = document.createElement("a")
        a.href = url;
        a.target = "_blank"
        a.click()
    }
    return (
        <div className={className.wrapper}>
            <img
                onClick={handleClick}
                className={className.image}
                src={image} />
            <div className={className.content}>
                <div className={className.title}
                    onClick={handleClick}>
                    {title}
                </div>
                <div className={className.infor}>
                    <img
                        className={className.logoPaper}
                        src={`http://localhost:7075/${paper.logo}`} />
                    <div className={className.time}>{b}</div>
                </div>
            </div>
        </div>
    )
}

export default PostBig