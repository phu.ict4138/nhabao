import PostBig from "../PostBig";
import PostRow from "../PostRow";
import className from "./className";

function PostMain({ posts = [] }) {

    const firstPost = posts[0];

    return (
        <div className={className.wrapper}>
            <PostBig
                {...firstPost} />
            <PostRow
                posts={posts.slice(1, 4)} />
        </div>
    );
}

export default PostMain