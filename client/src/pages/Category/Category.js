import { useNavigate } from "react-router-dom";
function Category() {
    const navigate = useNavigate();
    return (
        <>

            <div className="flex-1 flex flex-row flex-wrap w-full bg-slate-200">
                <div onClick={() => {
                    navigate("/category/thoisu-1")
                }}
                    className="w-1/4 text-xl text-center m-3">
                    Thời sự
                </div>
                <div
                    onClick={() => {
                        navigate("/category/kinhdoanh-2")
                    }}
                    className="w-1/4 text-xl text-center m-3">Kinh doanh</div>
                <div
                    onClick={() => {
                        navigate("/category/giaoduc-3")
                    }}
                    className="w-1/4 text-xl text-center m-3">Giáo dục</div>
                <div
                    onClick={() => {
                        navigate("/category/thethao-4")
                    }}
                    className="w-1/4 text-xl text-center m-3">Thể thao</div>
                <div
                    onClick={() => {
                        navigate("/category/suckhoe-5")
                    }} className="w-1/4 text-xl text-center m-3">Sức khoẻ</div>
                <div
                    onClick={() => {
                        navigate("/category/giaitri-6")
                    }}
                    className="w-1/4 text-xl text-center m-3">Giải trí</div>
                <div
                    onClick={() => {
                        navigate("/category/thegioi-7")
                    }}
                    className="w-1/4 text-xl text-center m-3">Thế giới</div>
                <div
                    onClick={() => {
                        navigate("/category/phapluat-8")
                    }}
                    className="w-1/4 text-xl text-center m-3">Pháp luật</div>
                <div
                    onClick={() => {
                        navigate("/category/doisong-9")
                    }}
                    className="w-1/4 text-xl text-center m-3">Đời sống</div>
                <div
                    onClick={() => {
                        navigate("/category/xe-10")
                    }}
                    className="w-1/4 text-xl text-center m-3">Xe</div>
                <div
                    onClick={() => {
                        navigate("/category/khoahoc-11")
                    }}
                    className="w-1/4 text-xl text-center m-3">Khoa học</div>
            </div>


        </>
    );
}

export default Category