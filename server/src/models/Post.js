import { db } from "../database/database.js";
import {
    DATE,
    INTEGER,
    STRING,
    TEXT,
} from "../config/config.js";

const Post = db.define(
    "post",
    {
        postId: {
            type: INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        categoryId: {
            type: INTEGER,
        },
        subcategoryId: {
            type: INTEGER,
        },
        paperId: {
            type: INTEGER,
        },
        url: {
            type: STRING,
        },
        title: {
            type: TEXT,
        },
        abstract: {
            type: TEXT,
        },
        content: {
            type: TEXT,
        },
        image: {
            type: STRING,
        },
        publish: {
            type: DATE
        },
    },
    {
        freezeTableName: true,
        paranoid: true
    }
);

export default Post