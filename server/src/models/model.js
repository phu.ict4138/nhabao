import { db } from "../database/database.js";

import Category from "./Category.js";
import SubCategory from "./SubCategory.js";
import Post from "./Post.js";
import Paper from "./Paper.js";

const createAssociation = () => {
    Post.belongsTo(Category, {
        foreignKey: "categoryId",
        as: "category"
    });
    Category.hasMany(Post, {
        foreignKey: "categoryId",
        as: "posts"
    });
    Post.belongsTo(SubCategory, {
        foreignKey: "subcategoryId",
        as: "subcategory"
    });
    SubCategory.hasMany(Post, {
        foreignKey: "subcategoryId",
        as: "posts"
    });
    Post.belongsTo(Paper, {
        foreignKey: "paperId",
        as: "paper"
    });
    Paper.hasMany(Post, {
        foreignKey: "paperId",
        as: "posts"
    });
    SubCategory.belongsTo(Category, {
        foreignKey: "categoryId",
        as: "category"
    });
    Category.hasMany(SubCategory, {
        foreignKey: "categoryId",
        as: "subcategories"
    })
}

const initModel = async (force = false, alter = false) => {
    await db.sync({
        force: force,
        alter: alter,
    });

    createAssociation();

    await db.sync({
        force: force,
        alter: alter,
    });
}

export default initModel