import { db } from "../database/database.js";
import {
    INTEGER,
    STRING,
    TEXT,
} from "../config/config.js";

const Category = db.define(
    "category",
    {
        categoryId: {
            type: INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: STRING
        },
        description: {
            type: TEXT
        }
    },
    {
        freezeTableName: true,
        paranoid: true
    }
);

export default Category