import { db } from "../database/database.js";
import {
    INTEGER,
    STRING,
    TEXT,
} from "../config/config.js";

const SubCategory = db.define(
    "subcategory",
    {
        subcategoryId: {
            type: INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        categoryId: {
            type: INTEGER,
        },
        name: {
            type: STRING
        },
        description: {
            type: TEXT
        }
    },
    {
        freezeTableName: true,
        paranoid: true
    }
);

export default SubCategory