import { db } from "../database/database.js";
import {
    INTEGER,
    STRING,
    TEXT,
} from "../config/config.js";

const Paper = db.define(
    "paper",
    {
        paperId: {
            type: INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false,
        },
        name: {
            type: STRING
        },
        url: {
            type: STRING
        },
        logo: {
            type: STRING
        },
        description: {
            type: TEXT
        }
    },
    {
        freezeTableName: true,
        paranoid: true
    }
);

export default Paper