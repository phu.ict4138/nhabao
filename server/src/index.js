import express from "express";

import configServer, { PORT } from "./config/config.js";
import connectToDatabase from "./database/database.js";
import initModel from "./models/Model.js";
import initApiRouter from "./routers/router.js";

const app = express();

configServer(app);
await connectToDatabase();
await initModel(false, true);
initApiRouter(app);

app.get("/", async (req, res) => {
    return res.status(200).json({
        result: "success",
        data: "hello"
    })
});

app.listen(PORT, () => {
    console.log("Nhàbáo is running at port " + PORT)
})