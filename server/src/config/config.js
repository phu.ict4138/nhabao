import express from "express";
import cors from "cors";
import { DataTypes, QueryTypes, Op } from "sequelize";

const HOST = "localhost";
const PORT = "7075";
const DATABASE_NAME = "nhabao";
const USERNAME = "root";
const PASSWORD = null;
const DATABASE_TYPE = "mysql";
const IS_LOG = false;
const TIME_ZONE = "+07:00";
const TOKEN_SECRET_KEY = "hello";
const TOKEN_DURATION = "15s";
const REFRESH_TOKEN_SECRET_KEY = "xinchao";
const REFRESH_TOKEN_DURATION = "1h";
const SUCCESS = "success";
const FAIL = "fail";
const ERROR_SERVER = "Error from server";
const MISSING_PARAM = "Missing param";
const INVALID_PARAM = "Invalid param";
const NOT_EXIST = "Not exist";

const {
    INTEGER,
    STRING,
    DATE,
    TEXT,
    BOOLEAN,
    JSON,

} = DataTypes;

const {
    SELECT, DELETE, UPDATE, INSERT
} = QueryTypes

const configServer = (app) => {
    app.use(express.json());
    app.use(express.urlencoded({
        extended: true,
        limit: "50mb"
    }));
    app.use(cors());
    app.use(express.static("./"));
}

export default configServer
export {
    HOST,
    PORT,
    DATABASE_NAME,
    DATABASE_TYPE,
    PASSWORD,
    USERNAME,
    IS_LOG,
    TIME_ZONE,
    TOKEN_SECRET_KEY,
    TOKEN_DURATION,
    REFRESH_TOKEN_SECRET_KEY,
    REFRESH_TOKEN_DURATION,
    SUCCESS,
    ERROR_SERVER,
    FAIL,
    MISSING_PARAM,
    INVALID_PARAM,
    NOT_EXIST,
    INTEGER,
    STRING,
    DATE,
    TEXT,
    BOOLEAN,
    JSON,
    Op as Option
}