import { Sequelize } from "sequelize";

import {
    HOST,
    DATABASE_NAME,
    DATABASE_TYPE,
    PASSWORD,
    USERNAME,
    IS_LOG,
    TIME_ZONE
} from "../config/config.js";

const sequelize = new Sequelize(DATABASE_NAME, USERNAME, PASSWORD, {
    host: HOST,
    dialect: DATABASE_TYPE,
    logging: IS_LOG,
    timezone: TIME_ZONE
});

const connectToDatabase = async () => {
    try {
        await sequelize.authenticate();
        console.log("Connect to database success!");
    } catch (error) {
        console.log(error);
    }
}

export default connectToDatabase
export { sequelize as db }

