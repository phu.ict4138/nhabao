import homeService from "../services/Home.js";

const getOne = async (request, response) => {
    const { categoryId } = request.params;
    const {
        statusCode,
        result,
        message,
        category
    } = await categoryService.getOneCategory(categoryId);
    return response.status(statusCode).json({
        result,
        message,
        category
    });
}

const getHomePage = async (request, response) => {
    const {
        limit,
        page
    } = request.query;
    const {
        statusCode,
        result,
        message,
        posts,
        total
    } = await homeService.getHomePage(limit, page);
    return response.status(statusCode).json({
        result,
        message,
        total,
        posts
    });
}

const getAll = async (request, response) => {

}
const create = async (request, response) => {

}

const update = async (request, response) => {

}

const deletes = async (request, response) => {

}

const getMapping = async (request, response) => {
    const mapping = await homeService.getMapping();
    return response.status(200).json({
        result: "success",
        mapping: mapping
    });
}




const homeController = {
    getHomePage,
    getMapping
}

export default homeController