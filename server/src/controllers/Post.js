import postService from "../services/Post.js";


const getOne = async (request, response) => {
    const { postId } = request.params;
    const {
        statusCode,
        result,
        message,
        post
    } = await postService.getOnePost(postId);
    return response.status(statusCode).json({
        result,
        message,
        post
    });
}

const getAll = async (request, response) => {
}
const create = async (request, response) => {
    const {
        categoryId,
        subcategoryId,
        paperId,
        url,
        title,
        abstract,
        content,
        image,
        publish
    } = request.body;
    const {
        statusCode,
        result,
        message,
        post
    } = await postService.createPost(
        categoryId,
        subcategoryId,
        paperId,
        url,
        title,
        abstract,
        content,
        image,
        publish
    );
    return response.status(statusCode).json({
        result,
        message,
        post
    });
}

const update = async (request, response) => {

}

const deletes = async (request, response) => {

}
const search = async (request, response) => {
    const {
        categoryId,
        subcategoryId,
        paperId,
        limit,
        page
    } = request.query;
    const {
        statusCode,
        result,
        message,
        posts,
        total
    } = await postService.search(
        categoryId,
        subcategoryId,
        paperId,
        limit,
        page
    );
    return response.status(statusCode).json({
        result,
        message,
        total,
        posts
    });
}

const getAllUrl = async (request, response) => {
    const {
        statusCode,
        result,
        message,
        urls
    } = await postService.getAllUrl();
    return response.status(statusCode).json({
        result,
        message,
        urls
    });
}

const postController = {
    create,
    getOne,
    getAll,
    update,
    delete: deletes,
    getAllUrl,
    search
}

export default postController