import categoryService from "../services/Category.js";


const getOne = async (request, response) => {
    const { categoryId } = request.params;
    const {
        statusCode,
        result,
        message,
        category
    } = await categoryService.getOneCategory(categoryId);
    return response.status(statusCode).json({
        result,
        message,
        category
    });
}


const getAll = async (request, response) => {
    const {
        statusCode,
        result,
        message,
        categories
    } = await categoryService.getAllCategory();
    return response.status(statusCode).json({
        result,
        message,
        categories
    });
}
const create = async (request, response) => {

}

const update = async (request, response) => {

}

const deletes = async (request, response) => {

}




const categoryController = {
    create,
    getOne,
    getAll,
    update,
    delete: deletes
}

export default categoryController