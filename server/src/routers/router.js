import initCategoryRouter from "./Category.js";
import initPostRouter from "./Post.js";
import initHomeRouter from "./Home.js";

const initApiRouter = (app) => {
    initCategoryRouter(app);
    initPostRouter(app);
    initHomeRouter(app);
}

export default initApiRouter