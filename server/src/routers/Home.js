import { Router } from "express";

import homeController from "../controllers/Home.js";

const initHomeRouter = (app) => {
    const router = Router();

    router.get("/get-home-page", homeController.getHomePage);
    router.get("/get-mapping", homeController.getMapping);

    return app.use("/api/v1/home", router);
}

export default initHomeRouter