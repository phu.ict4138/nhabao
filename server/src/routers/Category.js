import { Router } from "express";

import categoryController from "../controllers/Category.js";

const initCategoryRouter = (app) => {
    const router = Router();

    router.post("/create", categoryController.create);
    router.get("/", categoryController.getAll);
    router.get("/:categoryId", categoryController.getOne);
    router.put("/update", categoryController.update);
    router.delete("/delete", categoryController.delete);

    return app.use("/api/v1/category", router);
}

export default initCategoryRouter