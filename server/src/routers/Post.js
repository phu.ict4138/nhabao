import { Router } from "express";

import postController from "../controllers/Post.js";

const initPostRouter = (app) => {
    const router = Router();

    router.get("/get-all-url", postController.getAllUrl);
    router.get("/search", postController.search);
    router.get("/", postController.getAll);
    router.post("/create", postController.create);
    router.get("/:postId", postController.getOne);
    router.put("/update", postController.update);
    router.delete("/delete", postController.delete);


    return app.use("/api/v1/post", router);
}

export default initPostRouter