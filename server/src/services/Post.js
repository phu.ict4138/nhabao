import Category from "../models/Category.js";
import Paper from "../models/Paper.js";
import Post from "../models/Post.js";
import SubCategory from "../models/SubCategory.js";
import service, { errorServer, invalidParam, missingParam, notExist, success } from "./service.js";

const convertDate = (s) => {
    if (s == "") {
        return null;
    }
    const day = s.slice(0, 2);
    const month = s.slice(3, 5);
    const _ = s.slice(5)
    const _s = `${month}-${day}${_}`;
    return new Date(_s);
}

const getOnePost = async (postId) => {
    const post = await Post.findOne({
        where: {
            postId: postId
        },
        include: [
            { model: Category, as: "category" },
            { model: SubCategory, as: "subcategory" },
            { model: Paper, as: "paper" },
        ],
        attributes: {
            exclude: ["content", "categoryId", "subcategoryId", "paperId"]
        }
    });
    return {
        ...success,
        post
    }
}

const createPost = async (
    categoryId,
    subcategoryId,
    paperId,
    url,
    title,
    abstract,
    content,
    image,
    publish
) => {
    if (typeof (categoryId) == "undefined") {
        return missingParam;
    }
    if (typeof (subcategoryId) == "undefined") {
        return missingParam;
    }
    if (typeof (paperId) == "undefined") {
        return missingParam;
    }
    if (typeof (url) == "undefined") {
        return missingParam;
    }
    if (typeof (title) == "undefined") {
        return missingParam;
    }
    const _abstract = typeof (abstract) == "undefined" ? null : abstract;
    const _content = typeof (content) == "undefined" ? null : content;
    const _image = typeof (image) == "undefined" ? null : image;
    const _publish = typeof (publish) == "undefined" ? null : convertDate(publish);
    const post = await Post.create({
        categoryId,
        subcategoryId,
        paperId,
        url,
        title,
        abstract: _abstract,
        content: _content,
        image: _image,
        publish: _publish
    });
    return {
        ...success,
        post
    }
}

const getAllUrl = async () => {
    const posts = await Post.findAll({
        attributes: ["url"]
    });
    return {
        ...success,
        urls: posts.map(i => i.url)
    }
}

const search = async (
    categoryId,
    subcategoryId,
    paperId,
    limit,
    page
) => {
    const _limit = limit ? Number(limit) : 10;
    const _page = page ? Number(page) : 0;
    const where = JSON.parse(JSON.stringify({
        categoryId: categoryId,
        subcategoryId: subcategoryId,
        paperId: paperId
    }))
    const { count, rows } = await Post.findAndCountAll({
        where: where,
        limit: _limit,
        offset: _page * _limit,
        include: [
            { model: Category, as: "category" },
            { model: SubCategory, as: "subcategory" },
            { model: Paper, as: "paper" },
        ],
        order: [["publish", "DESC"]],
        attributes: {
            exclude: ["content", "categoryId", "subcategoryId", "paperId"]
        }
    });
    return {
        ...success,
        total: count,
        posts: rows
    }
}


const postService = {
    getOnePost,
    createPost,
    getAllUrl,
    search
}

export default postService