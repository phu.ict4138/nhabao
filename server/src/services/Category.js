import Category from "../models/Category.js";
import SubCategory from "../models/SubCategory.js";
import service, { errorServer, invalidParam, missingParam, notExist, success } from "./service.js";

const getOneCategory = async (categoryId) => {
    const category = await Category.findOne({
        where: {
            categoryId: categoryId
        },
        include: {
            model: SubCategory,
            as: "subcategories"
        }
    });
    return {
        ...success,
        category
    }
}

const getAllCategory = async () => {
    const categories = await Category.findAll({
        include: {
            model: SubCategory,
            as: "subcategories"
        }
    });
    return {
        ...success,
        categories
    }
}


const categoryService = {
    getOneCategory,
    getAllCategory
}

export default categoryService