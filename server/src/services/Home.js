import Category from "../models/Category.js"
import Paper from "../models/Paper.js";
import Post from "../models/Post.js";
import SubCategory from "../models/SubCategory.js";
import { success } from "./service.js";

const getMapping = async () => {
    const categories = await Category.findAll();
    const subcategories = await SubCategory.findAll();
    const papers = await Paper.findAll()
    const mapping = {
        category: {},
        subcategory: {},
        paper: {}
    };
    for (const i of categories) {
        mapping.category[i.name] = i.categoryId;
    }
    for (const i of subcategories) {
        mapping.subcategory[i.name] = i.subcategoryId;
    }
    for (const i of papers) {
        mapping.paper[i.url] = i.paperId;
    }
    return mapping;
}

const getHomePage = async (limit, page) => {
    const _limit = limit ? Number(limit) : 10;
    const _page = page ? Number(page) : 0;
    const { count, rows } = await Post.findAndCountAll({
        limit: _limit,
        offset: _page * _limit,
        include: [
            { model: Category, as: "category" },
            { model: SubCategory, as: "subcategory" },
            { model: Paper, as: "paper" },
        ],
        order: [["publish", "DESC"]],
        attributes: {
            exclude: ["content", "categoryId", "subcategoryId", "paperId"]
        }
    });
    return {
        ...success,
        total: count,
        posts: rows
    }
}

const homeService = {
    getMapping,
    getHomePage
}

export default homeService