import { unlink } from "fs";

import { ERROR_SERVER, FAIL, INVALID_PARAM, MISSING_PARAM, NOT_EXIST, SUCCESS } from "../config/config.js";

const missingParam = {
    statusCode: 300,
    result: FAIL,
    message: MISSING_PARAM
}

const invalidParam = {
    statusCode: 300,
    result: FAIL,
    message: INVALID_PARAM
}

const success = {
    statusCode: 200,
    result: SUCCESS
}

const errorServer = {
    statusCode: 500,
    result: FAIL,
    message: ERROR_SERVER
}

const notExist = {
    statusCode: 300,
    result: FAIL,
    message: NOT_EXIST
}

const getRoute = (baseUrl) => {
    const words = baseUrl.split("/");
    return words[3] ?? "unknown";
}

const getFileType = (mimetype) => {
    if (mimetype.startsWith("image")) {
        return "images";
    }
    if (mimetype.startsWith("video")) {
        return "videos";
    }
    return "others"
}

const removeFile = (path) => {
    try {
        unlink(path);
    } catch (error) {

    }
}

const removeFiles = (files) => {
    for (const file of file) {
        removeFile(file.path)
    }
}

const random = () => {
    const x = Math.random().toString();
    return x.slice(2)
}

const service = {
    missingParam,
    invalidParam,
    success,
    errorServer,
    notExist,
    getRoute,
    getFileType,
    removeFiles,
    removeFile,
    random
}

export default service
export {
    missingParam,
    invalidParam,
    success,
    errorServer,
    notExist,
    removeFiles,
    removeFile,
    random
}

